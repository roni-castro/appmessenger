# AppMessenger

## Backend

The backend project is a local Azure Function that provide a json payload for app.

### Table of contents:

- [Getting started backend](#getting-started-backend)

### Getting started backend

Access the project backend

```
cd AppMessenger
```

Install dependencies

```
npx yarn
```

Build project

```
npx yarn build
```

Run local

```
npx yarn start
```

## App Mobile

The App react-native mobile that provide messages.

### Table of contents:

- [Getting started mobile](#getting-started-mobile)
- [Tests](#tests)

### Getting started mobile

Access the project backend

```
cd AppMessengerRN
```

Install dependencies

```
npx yarn
```

Install pods ios project

```
cd ios && pod install && cd ..
```

Run iOS project

```
npx yarn ios
```

Run Android project

```
npx yarn android
```

### Tests

Run tests on mobile project

```
npx yarn test
```

Update snapshots

```
npx yarn test -u
```

Run tests coverage

```
npx yarn test --coverage
```

Coverage tests metrics

```
Metrics

{
    "branches": 50,
    "functions": 47,
    "lines": 76,
    "statements": 72
}

Actualy

{
    "branches": 50,
    "functions": 47.83,
    "lines": 76.19,
    "statements": 72.73
}
```

## TODOS

- Update message status to read from list;
- CI;
- Code quality metrics.
