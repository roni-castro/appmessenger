import React from 'react';
import {render} from 'react-native-testing-library';
import Home from './';
import {MockedNavigator} from '../../../__mocks__';

describe('Home', () => {
  it('renders correctly', () => {
    const {toJSON} = render(<MockedNavigator component={Home} />);
    expect(toJSON).toMatchSnapshot();
  });
});
