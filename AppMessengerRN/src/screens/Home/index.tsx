import React, {useEffect, useState} from 'react';
import {StatusBar, FlatList} from 'react-native';
import moment from 'moment';

import {Header} from '../../components/Header';
import {MessageContainer} from '../../components/MessageContainer';
import {Conatiner} from './styles';
import {IObjectDefault} from '../../interfaces';
import {useNavigation} from '@react-navigation/native';
import {getMessages} from '../../services';

const Home = () => {
  const [data, setData] = useState<IObjectDefault>();
  const [loading, setLoading] = useState(true);
  const [refresh, setRefresh] = useState(false);

  const navigation = useNavigation();

  const getOrderMessages = (messages?: IObjectDefault) => {
    return (
      messages &&
      messages.sort((a: IObjectDefault, b: IObjectDefault) =>
        moment(new Date(a.timestamp * 1000)).isBefore(
          new Date(b.timestamp * 1000),
        ),
      )
    );
  };

  const onRefrresh = () => {
    setRefresh(true);
    setLoading(true);
    getMessages().then((messages) => {
      setData(messages);
      setTimeout(() => {
        setRefresh(false);
      }, 3000);
      setTimeout(() => setLoading(false), 3000);
    });
  };

  useEffect(() => {
    getMessages().then((messages) => {
      setData(messages);
      setTimeout(() => setLoading(false), 3000);
    });
  }, []);

  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor="#f4f5f6" />
      <Conatiner>
        <Header />
        <FlatList
          data={getOrderMessages(data)}
          renderItem={(item) => MessageContainer(item, loading, {navigation})}
          keyExtractor={(item) => item.key}
          refreshing={refresh}
          onRefresh={onRefrresh}
          extraData={getOrderMessages(data)}
        />
      </Conatiner>
    </>
  );
};

export default Home;
