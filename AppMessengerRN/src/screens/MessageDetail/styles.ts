import styled from 'styled-components/native';
import {Platform} from 'react-native';

export const ContainerMessages = styled.SafeAreaView`
  margin-top: 20px;
  margin-horizontal: 15px;
`;

export const HeaderContainer = styled.SafeAreaView`
  height: ${Platform.OS === 'android' ? '50px' : '95px'};
  align-items: center;
  flex-direction: row;
`;

export const TextHeaderContainer = styled.View`
  width: 80%;
  align-items: flex-end;
`;
