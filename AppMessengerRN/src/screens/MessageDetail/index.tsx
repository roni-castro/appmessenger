import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

import {
  TextHeaderContainer,
  HeaderContainer,
  ContainerMessages,
} from './styles';
import {MessageBox} from '../../components/MessageBox';
import {H2, ParagraphLight} from '../../components/Typography';
import {useNavigation, useRoute} from '@react-navigation/native';

export const MessageDetail = () => {
  const navigation = useNavigation();
  const {item}: any = useRoute().params;

  return (
    <>
      <HeaderContainer>
        <Icon.Button
          name="back"
          size={30}
          backgroundColor="#f4f5f6"
          color="#69696a"
          activeOpacity={0.6}
          underlayColor="#DDDDDD"
          onPress={() => navigation.goBack()}
        />
        <TextHeaderContainer>
          <ParagraphLight>
            {moment(new Date(item.timestamp * 1000)).format(
              'DD/MM/YYYY - HH:MM',
            )}
          </ParagraphLight>
        </TextHeaderContainer>
      </HeaderContainer>
      <ContainerMessages>
        <H2>{item.subject}</H2>
        <MessageBox message={item.detail} />
      </ContainerMessages>
    </>
  );
};
