import React from 'react';
import {render} from 'react-native-testing-library';
import {MockedNavigator} from '../../../__mocks__';
import {MessageDetail} from './';

describe('MessageDetail', () => {
  it('renders correctly', () => {
    const {toJSON} = render(
      <MockedNavigator
        params={{item: {timestamp: 123, subject: '', detail: ''}}}
        component={MessageDetail}
      />,
    );
    expect(toJSON).toMatchSnapshot();
  });
});
