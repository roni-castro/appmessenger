import {View} from 'react-native';
import React from 'react';
import {render} from 'react-native-testing-library';
import {Skeleton} from './';

describe('Skeleton', () => {
  it('renders correctly', () => {
    const {toJSON} = render(<Skeleton isLoading={true} children={<View />} />);
    expect(toJSON).toMatchSnapshot();
  });
});
