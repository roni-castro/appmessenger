import React from 'react';
import {Dimensions} from 'react-native';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import LinearGradient from 'react-native-linear-gradient';
import {ISkeleton} from './interface';

const screenWidth = Dimensions.get('window').width - 48;
export const Skeleton = ({isLoading, children}: ISkeleton) => {
  return (
    <ShimmerPlaceHolder
      LinearGradient={LinearGradient}
      width={screenWidth}
      height={100}
      visible={isLoading}>
      {children}
    </ShimmerPlaceHolder>
  );
};
