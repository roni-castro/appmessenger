import React from 'react';

export interface ISkeleton {
  isLoading: boolean;
  children: React.ReactChild | React.ReactChild[];
}
