import {IObjectDefault} from '../../interfaces';

export interface IMessageContainer {
  item: IObjectDefault;
  index: number;
}
