import {Platform} from 'react-native';
import styled from 'styled-components/native';

export const H1 = styled.Text`
  font-family: ${Platform.OS === 'android' ? 'Nunito Bold' : 'Nunito-Bold'};
  font-size: 35px;
`;

export const H2 = styled.Text`
  font-family: ${Platform.OS === 'android' ? 'Nunito Bold' : 'Nunito-Bold'};
  font-size: 18px;
  color: #333;
`;

export const Paragraph = styled.Text`
  font-family: ${Platform.OS === 'android'
    ? 'Nunito Regular'
    : 'Nunito-Regular'};
  font-size: 16px;
  color: #6c6c6c;
`;

export const ParagraphLight = styled.Text`
  font-family: ${Platform.OS === 'android' ? 'Nunito Light' : 'Nunito-Light'};
  font-size: 12px;
  color: #69696a;
`;

export const ParagraphBold = styled.Text`
  font-family: ${Platform.OS === 'android' ? 'Nunito Bold' : 'Nunito-Bold'};
  font-size: 16px;
  color: #6c6c6c;
`;
