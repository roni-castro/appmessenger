import React from 'react';
import {render} from 'react-native-testing-library';
import {Header} from './';

describe('Header', () => {
  it('renders correctly', () => {
    const {toJSON} = render(<Header />);
    expect(toJSON).toMatchSnapshot();
  });
});
