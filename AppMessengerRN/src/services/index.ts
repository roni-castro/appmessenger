import axios from 'axios';
import {Platform} from 'react-native';

export const getMessages = () => {
  const uri =
    Platform.OS === 'android'
      ? 'http://10.0.2.2:7071/api/message'
      : 'http://0.0.0.0:7071/api/message';

  return axios.get(uri).then((response) => response.data.messages);
};
